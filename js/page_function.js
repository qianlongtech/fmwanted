(function ($, Parallax) {
    "use strict";
    var GO = {};
    GO.ele = {
        body: $("body"),
        scroll: $(".fullpage"),
        stay: $(".stay"),
        staySection: $(".stay .section"),
        fullSection: $(".fullpage .section"),
        player: $(".player"),
        audio: $("audio"),
        next: $(".next")
    };
    GO.sectionID = [0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6];
    GO.bgColor = ["#848acb", "#858ccd", "#f67f52", "#5bb0f2", "#febf62", "#02947d", "#03947e"];
    GO.showImg = function (collec, type) {
        collec.each(function (i, e) {
            if (type) {
                $(this).delay((i + 1) * 500).fadeIn();
            } else {
                $(this).fadeIn();
            }
        });
    };
    GO.showStaySection = function (index) {
        GO.ele.staySection.eq(index).fadeIn();
        GO.ele.staySection.eq(index).siblings().fadeOut();
        GO.showImg(GO.ele.staySection.eq(index).find("img"), 0);
    };
    GO.showScrollSection = function (index) {
        GO.showImg(GO.ele.fullSection.eq(index).find("img"), 1);
    };
    GO.onLeave = function (index, nextIndex, direction) {
        GO.ele.fullSection.find("img").fadeOut(200);
        if (GO.sectionID[nextIndex - 1] !== GO.sectionID[index - 1]) {
            GO.ele.staySection.find("img").fadeOut(200);
        }
    };
    GO.onLoad = function (anchorLink, index) {
        GO.ele.body.css("background-color", GO.bgColor[GO.sectionID[index - 1]]);
        GO.showStaySection(GO.sectionID[index - 1]);
        GO.showScrollSection(index - 1);
        if (index === GO.ele.fullSection.length) {
            GO.ele.next.hide();
        } else {
            GO.ele.next.show();
        }
    };
    GO.onPause = function () {
        var aim = GO.ele.audio[0];
        if (aim.paused) {
            aim.play();
        } else {
            aim.pause();
        }
    };
    GO.ele.player.click(GO.onPause);
    GO.ele.scroll.fullpage({
        sectionSelector: ".fullpage .section",
        resize : true,
        onLeave: GO.onLeave,
        afterLoad: GO.onLoad
    });
}(window.jQuery, window.Parallax));